# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="GPP is a general-purpose preprocessor with customizable syntax "
HOMEPAGE="https://logological.org/gpp"
SRC_URI="http://files.nothingisreal.com/software/${PN}/${P}.tar.bz2"

LICENSE="LGPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
